import 'package:data_logger_dashboard/pages/res1280.dart';
import 'package:data_logger_dashboard/pages/res1440.dart';
import 'package:data_logger_dashboard/pages/res1920.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Data Logger Dashboard',
      theme: ThemeData(
        fontFamily: 'NotoSans',
      ),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (width <= 1280) {
      return const Res1280();
    } else if (width <= 1440) {
      return const Res1440();
    }
    return const Res1920();
  }
}
