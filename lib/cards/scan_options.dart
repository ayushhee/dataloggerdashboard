import 'package:data_logger_dashboard/reusable components/buttons.dart';
import 'package:data_logger_dashboard/reusable components/outer_conatiner.dart';
import 'package:flutter/material.dart';
import 'package:data_logger_dashboard/constants.dart';
import 'package:data_logger_dashboard/reusable components/border_container.dart';

class ScanOptions extends StatefulWidget {
  final double height;

  const ScanOptions({this.height = 300});

  @override
  _ScanOptionsState createState() => _ScanOptionsState();
}

class _ScanOptionsState extends State<ScanOptions> {
  @override
  Widget build(BuildContext context) {
    return CardWidget2(
      height: widget.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Scan Options',
            style: TextStyle(
                fontSize: 24, color: Colors.blue, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 32),
          const Text(
            'Scan Interval:',
            style: TextStyle(color: heading),
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              const Text('Short: ', style: TextStyle(color: heading)),
              CustomContainer(
                  text: '10',
                  width: MediaQuery.of(context).size.width * (0.03)),
              const SizedBox(width: 4),
              const Text('sec', style: TextStyle(color: sub)),
            ],
          ),
          const SizedBox(height: 4),
          Row(
            children: [
              const Text('Long:  ', style: TextStyle(color: heading)),
              CustomContainer(
                  text: '10',
                  width: MediaQuery.of(context).size.width * (0.03)),
              const SizedBox(width: 4),
              const Text('Hr', style: TextStyle(color: sub)),
              const SizedBox(width: 16),
              CustomContainer(
                  text: '44',
                  width: MediaQuery.of(context).size.width * (0.03)),
              const SizedBox(width: 3),
              const Text('min', style: TextStyle(color: sub)),
            ],
          ),
          const SizedBox(height: 8),
          const Text(
            'Next Scan Stary Interval:',
            style: TextStyle(color: heading),
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              CustomContainer(
                  text: '10',
                  width: MediaQuery.of(context).size.width * (0.03)),
              const SizedBox(width: 4),
              const Text('Hr', style: TextStyle(color: sub)),
              const SizedBox(width: 16),
              CustomContainer(
                  text: '44',
                  width: MediaQuery.of(context).size.width * (0.03)),
              const SizedBox(width: 3),
              const Text('min', style: TextStyle(color: sub)),
            ],
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: CustomButton2(
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height * 0.065,
                color: heading,
                text: 'Update',
                onPressed: () {},
              ),
            ),
          ),
        ],
      ),
    );
  }
}
