import 'package:data_logger_dashboard/reusable components/buttons.dart';
import 'package:data_logger_dashboard/reusable components/outer_conatiner.dart';
import 'package:flutter/material.dart';
import 'package:icons_plus/icons_plus.dart';
import 'package:data_logger_dashboard/constants.dart';

String dropdownvalue = 'ESDL';

var items = [
  'ESDL',
  'Item 2',
  'Item 3',
];

//input box container
class CustomContainer extends StatelessWidget {
  final String text;

  const CustomContainer({
    super.key,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.8,
      height: 30,
      padding: const EdgeInsets.all(5.5),
      decoration: BoxDecoration(
        border: Border.all(color: heading, width: 2),
        borderRadius: BorderRadius.circular(7),
      ),
      child: TextField(
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: text,
          hintStyle: const TextStyle(
            fontSize: 13,
            color: Color.fromARGB(255, 139, 144, 153),
          ),
        ),
        style: const TextStyle(
          fontSize: 13,
          color: sub,
        ),
      ),
    );
  }
}

class OpenSite extends StatefulWidget {
  final double height;

  const OpenSite({this.height = 300});
  @override
  _OpenSiteState createState() => _OpenSiteState();
}

class _OpenSiteState extends State<OpenSite> {
  @override
  Widget build(BuildContext context) {
    return CardWidget(
      height: widget.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Open Site',
            style: TextStyle(
                fontSize: 24, color: Colors.blue, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 30),
          const Text(
            'Select Site:',
            style: TextStyle(color: heading),
          ),
          const SizedBox(
            height: 3,
          ),
          Container(
            width: MediaQuery.of(context).size.width * (0.8),
            height: 30,
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              border: Border.all(color: heading, width: 2),
              borderRadius: BorderRadius.circular(7),
            ),
            child: DropdownButton(
              value: dropdownvalue,
              icon: const Icon(
                Bootstrap.caret_down_fill,
                size: 15,
                color: sub,
              ),
              style: const TextStyle(
                  fontSize: 13, color: Color.fromARGB(255, 139, 144, 153)),
              isExpanded: true,
              underline: const SizedBox(),
              dropdownColor: maincolor,
              items: items.map((String items) {
                return DropdownMenuItem(
                  value: items,
                  child: Text(items),
                );
              }).toList(),
              onChanged: (String? newValue) {
                setState(() {
                  dropdownvalue = newValue!;
                });
              },
            ),
          ),
          const SizedBox(
            height: 7,
          ),
          const Text(
            'Site Description:',
            style: TextStyle(color: heading),
          ),
          const SizedBox(
            height: 3,
          ),
          const CustomContainer(text: 'Check Site'),
          const SizedBox(
            height: 7,
          ),
          const Text(
            'Logger Serial Number(Register)',
            style: TextStyle(color: heading),
          ),
          const SizedBox(
            height: 3,
          ),
          const CustomContainer(text: '__1710182'),
          const SizedBox(
            height: 7,
          ),
          const Text(
            'Logger Serial Number Found:',
            style: TextStyle(color: heading),
          ),
          const SizedBox(
            height: 3,
          ),
          const CustomContainer(text: '__1710182'),
          const SizedBox(
            height: 10,
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  CustomButton2(
                      text: 'Create Site',
                      onPressed: () {},
                      width: MediaQuery.of(context).size.width * 0.09,
                      height: MediaQuery.of(context).size.height * 0.065,
                      color: heading),
                  CustomButton2(
                    text: 'Close Site',
                    onPressed: () {},
                    width: MediaQuery.of(context).size.width * 0.09,
                    height: MediaQuery.of(context).size.height * 0.065,
                    color: red,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
