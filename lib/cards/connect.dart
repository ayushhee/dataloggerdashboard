import 'package:data_logger_dashboard/reusable%20components/outer_conatiner.dart';
import 'package:flutter/material.dart';
import 'package:data_logger_dashboard/constants.dart';
import 'package:data_logger_dashboard/reusable components/buttons.dart';

class Connect extends StatelessWidget {
  final double height;
  const Connect({super.key, this.height = 300});

  @override
  Widget build(BuildContext context) {
    return CardWidget(
      height: height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Connect',
            style: TextStyle(
                fontSize: 24, color: Colors.blue, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 32),
          const Text(
            'Available Ports',
            style: TextStyle(color: sub),
          ),
          const SizedBox(height: 16),
          const Text(
            'COM21 is Connected',
            style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(253, 254, 254, 1)),
          ),
          const SizedBox(height: 16),
          RichText(
              text: const TextSpan(children: <TextSpan>[
            TextSpan(
                text: 'Manufacturet Name: ', style: TextStyle(color: heading)),
            TextSpan(text: 'Encardio Rite', style: TextStyle(color: sub)),
          ])),
          const SizedBox(height: 8),
          RichText(
              text: const TextSpan(children: <TextSpan>[
            TextSpan(text: 'Product Name: ', style: TextStyle(color: heading)),
            TextSpan(text: 'ESDL-32UNI0_DL', style: TextStyle(color: sub)),
          ])),
          const SizedBox(height: 8),
          RichText(
              text: const TextSpan(children: <TextSpan>[
            TextSpan(text: 'Vendor id: ', style: TextStyle(color: heading)),
            TextSpan(text: '1155         ', style: TextStyle(color: sub)),
            TextSpan(text: 'Product ID: ', style: TextStyle(color: heading)),
            TextSpan(text: '22336', style: TextStyle(color: sub)),
          ])),
          const SizedBox(height: 8),
          RichText(
              text: const TextSpan(children: <TextSpan>[
            TextSpan(
                text: 'Battery Voltage: ', style: TextStyle(color: heading)),
            TextSpan(text: '0.0', style: TextStyle(color: sub)),
          ])),
          const SizedBox(height: 8),
          RichText(
              text: const TextSpan(children: <TextSpan>[
            TextSpan(
                text: 'Firewire Version: ', style: TextStyle(color: heading)),
            TextSpan(text: '', style: TextStyle(color: sub)),
          ])),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: CustomButton2(
                height: MediaQuery.of(context).size.height * 0.065,
                width: MediaQuery.of(context).size.width * 0.8,
                text: 'Disconnect',
                onPressed: () {},
                color: red,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
