import 'package:data_logger_dashboard/reusable components/outer_conatiner.dart';
import 'package:data_logger_dashboard/reusable components/border_container.dart';
import 'package:flutter/material.dart';
import 'package:data_logger_dashboard/constants.dart';
import 'package:data_logger_dashboard/reusable components/buttons.dart';

class ScanStatus extends StatefulWidget {
  final double height;

  const ScanStatus({this.height = 300});

  @override
  _ScanStatusState createState() => _ScanStatusState();
}

class _ScanStatusState extends State<ScanStatus> {
  @override
  Widget build(BuildContext context) {
    return CardWidget2(
      height: widget.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Scan Status:',
            style: TextStyle(
                fontSize: 24, color: Colors.blue, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 32),
          const Text(
            'Total Record:',
            style: TextStyle(
                fontSize: 15, fontWeight: FontWeight.w500, color: heading),
          ),
          CustomContainer(
            text: '',
            width: MediaQuery.of(context).size.width * (0.37),
          ),
          const SizedBox(height: 4),
          const Text(
            'Record from last download:',
            style: TextStyle(
                fontSize: 15, fontWeight: FontWeight.w500, color: heading),
          ),
          CustomContainer(
            text: '',
            width: MediaQuery.of(context).size.width * (0.37),
          ),
          const SizedBox(height: 4),
          const Text(
            'Record from last upload:',
            style: TextStyle(
                fontSize: 15, fontWeight: FontWeight.w500, color: heading),
          ),
          CustomContainer(
            text: '',
            width: MediaQuery.of(context).size.width * (0.37),
          ),
          const SizedBox(height: 4),
          const Text(
            'Status:',
            style: TextStyle(
                fontSize: 15, fontWeight: FontWeight.w500, color: heading),
          ),
          const SizedBox(height: 4),
          CustomContainer(
            text: '',
            width: MediaQuery.of(context).size.width * (0.37),
          ),
          const SizedBox(height: 4),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  CustomButton2(
                      text: 'Update',
                      onPressed: () {},
                      width: MediaQuery.of(context).size.width * 0.15,
                      height: MediaQuery.of(context).size.height * 0.065,
                      color: heading),
                  CustomButton2(
                    text: 'Start',
                    onPressed: () {},
                    width: MediaQuery.of(context).size.width * 0.15,
                    height: MediaQuery.of(context).size.height * 0.065,
                    color: green,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
