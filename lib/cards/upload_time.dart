import 'package:data_logger_dashboard/reusable components/outer_conatiner.dart';
import 'package:data_logger_dashboard/reusable components/border_container.dart';
import 'package:flutter/material.dart';
import 'package:data_logger_dashboard/constants.dart';

class UploadTime extends StatefulWidget {
  final double height;

  const UploadTime({super.key, this.height = 300});

  @override
  _UploadTimeState createState() => _UploadTimeState();
}

class _UploadTimeState extends State<UploadTime> {
  @override
  Widget build(BuildContext context) {
    return CardWidget(
      height: widget.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Upload Time',
            style: TextStyle(
                fontSize: 24, color: Colors.blue, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 32),
          const Text(
            'Upload Time Interval:',
            style: TextStyle(color: heading),
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              CustomContainer(
                  text: '10',
                  width: MediaQuery.of(context).size.width * (0.03)),
              const SizedBox(width: 4),
              const Text('Hr', style: TextStyle(color: sub)),
              const SizedBox(width: 16),
              CustomContainer(
                  text: '44',
                  width: MediaQuery.of(context).size.width * (0.03)),
              const SizedBox(width: 3),
              const Text('min', style: TextStyle(color: sub)),
            ],
          ),
          const SizedBox(height: 24),
          const Text(
            'Next Upload Time Start:',
            style: TextStyle(color: heading),
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              CustomContainer(
                  text: '10',
                  width: MediaQuery.of(context).size.width * (0.03)),
              const SizedBox(width: 4),
              const Text('Hr', style: TextStyle(color: sub)),
              const SizedBox(width: 16),
              CustomContainer(
                  text: '44',
                  width: MediaQuery.of(context).size.width * (0.03)),
              const SizedBox(width: 3),
              const Text('min', style: TextStyle(color: sub)),
            ],
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.height * 0.065,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                ),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: maincolor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                  onPressed: () {},
                  child: const Text(
                    'Update',
                    style: TextStyle(color: heading),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
