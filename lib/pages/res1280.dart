import 'package:flutter/material.dart';
import 'package:data_logger_dashboard/cards/scan_options.dart';
import 'package:data_logger_dashboard/cards/scan_status.dart';
import 'package:data_logger_dashboard/cards/upload_time.dart';
import 'package:data_logger_dashboard/reusable components/sidebar.dart';
import 'package:data_logger_dashboard/cards/connect.dart';
import 'package:data_logger_dashboard/cards/open_site.dart';
import 'package:data_logger_dashboard/constants.dart';

class Res1280 extends StatefulWidget {
  const Res1280({Key? key}) : super(key: key);

  @override
  Res1280State createState() => Res1280State();
}

class Res1280State extends State<Res1280> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: Row(
        children: [
          // Side Navbar
          const SideBar(),
          // Content
          Expanded(
            child: SingleChildScrollView(
              //to set maintheme to the entire content box
              child: Container(
                decoration: const BoxDecoration(
                  color: maincolor,
                ),
                //for rounded border
                child: Container(
                  margin: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: secondcolor,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Connect(
                                height:
                                    MediaQuery.of(context).size.height * 0.64),
                            OpenSite(
                                height:
                                    MediaQuery.of(context).size.height * 0.64),
                            UploadTime(
                                height:
                                    MediaQuery.of(context).size.height * 0.64),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ScanOptions(
                                height:
                                    MediaQuery.of(context).size.height * 0.64),
                            ScanStatus(
                                height:
                                    MediaQuery.of(context).size.height * 0.64),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
