import 'package:flutter/material.dart';
import 'package:data_logger_dashboard/cards/scan_options.dart';
import 'package:data_logger_dashboard/cards/scan_status.dart';
import 'package:data_logger_dashboard/cards/upload_time.dart';
import 'package:data_logger_dashboard/reusable%20components/sidebar.dart';
import 'package:data_logger_dashboard/cards/connect.dart';
import 'package:data_logger_dashboard/cards/open_site.dart';
import 'package:data_logger_dashboard/constants.dart';

class Res1920 extends StatefulWidget {
  const Res1920({Key? key}) : super(key: key);

  @override
  Res1920State createState() => Res1920State();
}

class Res1920State extends State<Res1920> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: Row(
        children: [
          // Side Navbar
          SideBar(
            width: MediaQuery.of(context).size.width * 0.23,
          ),
          // Content
          Expanded(
            child: SingleChildScrollView(
              //to set maintheme to the entire content box
              child: Container(
                decoration: const BoxDecoration(
                  color: maincolor,
                ),
                //for rounded border
                child: Container(
                  margin: const EdgeInsets.all(32),
                  padding: const EdgeInsets.only(top: 16, bottom: 16),
                  decoration: BoxDecoration(
                    color: secondcolor,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Connect(
                                height:
                                    MediaQuery.of(context).size.height * 0.48),
                            OpenSite(
                                height:
                                    MediaQuery.of(context).size.height * 0.48),
                            UploadTime(
                                height:
                                    MediaQuery.of(context).size.height * 0.48),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ScanOptions(
                                height:
                                    MediaQuery.of(context).size.height * 0.40),
                            ScanStatus(
                                height:
                                    MediaQuery.of(context).size.height * 0.40),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
