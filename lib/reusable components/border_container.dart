import 'package:flutter/material.dart';
import 'package:data_logger_dashboard/constants.dart';

class CustomContainer extends StatelessWidget {
  final String text;
  final double width;

  const CustomContainer({
    super.key,
    required this.text,
    this.width = 200.0,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 30,
      padding: const EdgeInsets.all(5.5),
      decoration: BoxDecoration(
        border: Border.all(color: heading, width: 2),
        borderRadius: BorderRadius.circular(7),
      ),
      child: TextField(
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: text,
          hintStyle: const TextStyle(
            fontSize: 13,
            color: Color.fromARGB(255, 139, 144, 153),
          ),
        ),
        style: const TextStyle(
          fontSize: 13,
          color: sub,
        ),
      ),
    );
  }
}
