import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  final Widget child;
  final double height;
  const CardWidget({Key? key, required this.child, this.height = 300})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: MediaQuery.of(context).size.width * 0.225,
      margin: const EdgeInsets.only(left: 10, right: 10, top: 16),
      padding: const EdgeInsets.all(24),
      decoration: BoxDecoration(
        color: const Color.fromRGBO(68, 71, 77, 1),
        borderRadius: BorderRadius.circular(20),
      ),
      child: child,
    );
  }
}

class CardWidget2 extends StatelessWidget {
  final double height;
  final Widget child;
  const CardWidget2({Key? key, required this.child, this.height = 300})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: MediaQuery.of(context).size.width * 0.345,
      margin: const EdgeInsets.only(left: 10, right: 10, top: 16, bottom: 16),
      padding: const EdgeInsets.all(24),
      decoration: BoxDecoration(
        color: const Color.fromRGBO(68, 71, 77, 1),
        borderRadius: BorderRadius.circular(20),
      ),
      child: child,
    );
  }
}
