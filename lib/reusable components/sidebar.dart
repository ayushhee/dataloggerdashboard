import 'package:data_logger_dashboard/reusable%20components/buttons.dart';
import 'package:flutter/material.dart';
import 'package:data_logger_dashboard/constants.dart';
import 'package:icons_plus/icons_plus.dart';

class SideBar extends StatelessWidget {
  final double width;
  const SideBar({
    super.key,
    this.width = 310.0,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      // width: MediaQuery.of(context).size.width * (0.28),
      width: width,
      color: maincolor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(top: 56, left: 16, right: 16),
            child: CustomButton(
              onPressed: () {},
              text: 'Dashboard',
              icon: Bootstrap.columns_gap,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 24, left: 16, right: 16),
            child: CustomButton(
              onPressed: () {},
              text: 'File',
              icon: Bootstrap.file_earmark,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 24, left: 16, right: 16),
            child: CustomButton(
              onPressed: () {},
              text: 'Edit',
              icon: Bootstrap.pencil_square,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 24, left: 16, right: 16),
            child: CustomButton(
              onPressed: () {},
              text: 'Data Logger',
              icon: Bootstrap.clipboard2_data,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 24, left: 16, right: 16),
            child: CustomButton(
              onPressed: () {},
              text: 'Configuration Sensor',
              icon: Bootstrap.puzzle,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 24, left: 16, right: 16),
            child: CustomButton(
              onPressed: () {},
              text: 'Diagnostic',
              icon: Bootstrap.layers,
            ),
          ),
        ],
      ),
    );
  }
}
