import 'package:flutter/material.dart';

const maincolor = Color.fromRGBO(36, 37, 45, 1);
const secondcolor = Color.fromRGBO(51, 52, 62, 1);
const red = Color.fromARGB(255, 228, 101, 92);
const green = Color.fromARGB(255, 119, 187, 114);
const sub = Color.fromARGB(255, 166, 178, 200);
const heading = Color.fromRGBO(215, 217, 250, 1);
